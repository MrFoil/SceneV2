//
// Created by DIko on 17.06.2015.
//

#include "DirLight.h"

DirLight::DirLight(vec4 direction, vec3 color, GLfloat ambientInt, GLfloat dirInt, GLuint shaderId) {
    this->direction   = direction;
    this->color       = color;
    this->ambInt      = ambientInt;
    this->dirInt      = dirInt;

    this->directionId = glGetUniformLocation(shaderId, "light.direction");
    this->colorId     = glGetUniformLocation(shaderId, "light.color");
    this->ambIntId    = glGetUniformLocation(shaderId, "light.ambientInt");
    this->dirIntId    = glGetUniformLocation(shaderId, "light.dirInt");
}

DirLight::~DirLight() {
    cout << "DirLight destructor";
}

void DirLight::render() {
    glUniform3f(directionId, direction.x, direction.y, direction.z);
    glUniform3f(colorId, color.x, color.y, color.z);
    glUniform1f(ambIntId, ambInt);
    glUniform1f(dirIntId, dirInt);
}