//
// Created by DIko on 26.06.2015.
//

#ifndef MY_SCENE_V__2_1_WALL_H
#define MY_SCENE_V__2_1_WALL_H

#include "../Classes/Entity.h"
#include "../hdr/CommonHeaders.h"
#include "../hdr/CommonMath.h"
#include "../hdr/CommonFunctions.h"


class Wall : public Entity {
private:
    Vertex *mesh;

protected:
    GLuint vao, vbo, shaderId, textureId, translationId;
    vec4 location = vec4(0.0f);

    GLuint createVbo(Vertex*, GLuint);
    void calcNormals(GLuint *indices, GLuint indCount, Vertex *vertices, GLuint vertCount);
public:
    Wall(vec3 a, vec3 b, vec3 c, vec3 d, GLuint shaderId, GLuint texDim);
    ~Wall();

    void setLocation(vec4 loc);
    void setTexture(char const *name);

    void render();
};


#endif //MY_SCENE_V__2_1_WALL_H
