//
// Created by DIko on 27.05.2015.
//

#include "Camera.h"

Camera::Camera(vec3 pos, Window *window) {
    this->pos = pos;
    this->window = window;

    mouseSpd  = 0.003f;
    camSpd    = 0.07f;
    camSpd    = 0.009f;
    horAngle  = 0.0f;
    vertAngle = 0.0f;

    cursorStartX = this->window->screenCentX;
    cursorStartY = this->window->screenCentY;
    RMB_DOWN = false;
}

Camera::~Camera() {

}

void Camera::calcMatrices(GLdouble dTime) {
    double xpos = window->screenCentX, ypos = window->screenCentY;




    if( glfwGetMouseButton( window->getWindow(), GLFW_MOUSE_BUTTON_RIGHT ) == GLFW_PRESS ){
        if(!RMB_DOWN){
            glfwGetCursorPos(window->getWindow(), &cursorStartX, &cursorStartY);
            RMB_DOWN = true;
        }
    }else{
        RMB_DOWN = false;
    }

    if(RMB_DOWN){
        glfwGetCursorPos(window->getWindow(), &xpos, &ypos);

        glfwSetCursorPos(window->getWindow(), cursorStartX, cursorStartY);

        horAngle    += mouseSpd * float( cursorStartX - xpos );
        vertAngle   += mouseSpd * float( cursorStartY - ypos );
        if (vertAngle > 3.14f/2.0f) vertAngle = 3.14f/2.0f;
        if (vertAngle < -3.14f/2.0f) vertAngle = -3.14f/2.0f;
    }

    glm::vec3 direction(
            cos(vertAngle) * sin(horAngle),
            sin(vertAngle),
            cos(vertAngle) * cos(horAngle)
    );

    // Right vector
    glm::vec3 right = glm::vec3(
            sin(horAngle - 3.14f/2.0f),
            0,
            cos(horAngle - 3.14f/2.0f)
    );

    // Up vector
    glm::vec3 up = glm::cross(right, direction);
    // Move forward
    if (glfwGetKey( window->getWindow(), GLFW_KEY_W ) == GLFW_PRESS || (glfwGetMouseButton( window->getWindow(), GLFW_MOUSE_BUTTON_LEFT ) == GLFW_PRESS && glfwGetMouseButton( window->getWindow(), GLFW_MOUSE_BUTTON_RIGHT ) == GLFW_PRESS )){
        pos += direction *  (camSpd + (GLfloat)dTime);
    }
    // Move backward
    if (glfwGetKey( window->getWindow(), GLFW_KEY_S ) == GLFW_PRESS){
        pos -= direction *  (camSpd + (GLfloat)dTime);

    }
    // Strafe right
    if (glfwGetKey( window->getWindow(), GLFW_KEY_D ) == GLFW_PRESS){
        pos += right * (camSpd + (GLfloat)dTime);
    }
    // Strafe left
    if (glfwGetKey( window->getWindow(), GLFW_KEY_A ) == GLFW_PRESS){
        pos -= right *  (camSpd + (GLfloat)dTime);
    }
    //up
    if (glfwGetKey( window->getWindow(), GLFW_KEY_E ) == GLFW_PRESS){
        pos += vec3(0.0f, 1.0f, 0.0f) *  (camSpd + (GLfloat)dTime);
    }

    if (glfwGetKey( window->getWindow(), GLFW_KEY_Q ) == GLFW_PRESS){
        pos += vec3(0.0f, -1.0f, 0.0f) *  (camSpd + (GLfloat)dTime);
    }

    projMat = perspective(45.0f, 4.0f / 3.0f, 0.1f, 50.0f);

    viewMat = lookAt(
            pos,                // Camera is here
            pos+direction,      // and looks here : at the same position, plus "direction"
            up                  // Head is up (set to 0,-1,ue to look upside-down)
    );

    modelMat = mat4(1.0f);

    mvp = projMat * viewMat * modelMat;
}

mat4 Camera::getMVP() {
    return mvp;
}

mat4 Camera::getProjMat() {
    return projMat;
}

mat4 Camera::getViewMat() {
    return viewMat;
}

mat4 Camera::getModelMat() {
    return modelMat;
}

void Camera::pushMatrix(mat4 matrix, const char *name, GLuint shaderId) {
    glUseProgram(shaderId);

    glUniformMatrix4fv(glGetUniformLocation(shaderId, name), 1, GL_FALSE, &matrix[0][0]);
}

void Camera::sendMVP(GLuint shaderId) {
    glUniformMatrix4fv(glGetUniformLocation(shaderId, "MVP"), 1, GL_FALSE, &mvp[0][0]);
}