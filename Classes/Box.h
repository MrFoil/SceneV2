//
// Created by DIko on 24.05.2015.
//

#ifndef MY_SCENE_V__2_1_BOX_H
#define MY_SCENE_V__2_1_BOX_H
#include "../hdr/CommonHeaders.h"
#include "../hdr/CommonMath.h"
#include "../hdr/CommonFunctions.h"
#include "../Classes/Entity.h"
#include "../Classes/Window.h"
using namespace std;
using namespace glm;



class Box : public Entity{
private:
    GLuint vao, vbo, shaderId, textureId;
    GLint translationId;

    vec4 position;

    Vertex vertices[8];
    Vertex *mesh;

protected:
    GLuint createVbo(Vertex*, GLuint);
    void calcNormals(GLuint *indices, GLuint indCount, Vertex *vertices, GLuint vertCount);

public:
    Box(vec3 point1, vec3 point2, GLuint shaderId);
    ~Box();

    void setTexture(const char*);
    void setPosition(vec4 newPos);

    void applyMovement(Window &window, GLfloat speed, GLdouble time);

    void render();
};


#endif //MY_SCENE_V__2_1_BOX_H
