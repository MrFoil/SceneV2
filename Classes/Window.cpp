//
// Created by DIko on 26.05.2015.
//

#include "Window.h"

Window::Window(GLuint width, GLuint heigh, char const* name, vec4 color) {
    this->width = width;
    this->heigh = heigh;
    this->name  = name;
    this->color = color;

    screenCentX = width/2;
    screenCentY = heigh/2;
}

Window::~Window() {

}

int Window::createWindow() {
    if( !glfwInit() )
    {
        fprintf( stderr, "Failed to initialize GLFW\n" );
        return -1;
    }

    glfwWindowHint(GLFW_SAMPLES, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    // Open a window and create its OpenGL context
    window = glfwCreateWindow( this->width, this->heigh, this->name, NULL, NULL);
    if( window == NULL ){
        fprintf( stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible. Try the 2.1 version of the tutorials.\n" );
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);


    glewExperimental = true;
    if (glewInit() != GLEW_OK) {
        fprintf(stderr, "Failed to initialize GLEW\n");
        return -1;
    }

    glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);
    glClearColor(this->color.x, this->color.y, this->color.z, this->color.w);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);


    return 0;
}

bool Window::closeWindow() {
    if(glfwGetKey(window, GLFW_KEY_ESCAPE ) == GLFW_PRESS && glfwWindowShouldClose(window) == 0){
       glfwTerminate();
       return 1;
    }
    return 0;
}


GLFWwindow* Window::getWindow() {
    return this->window;
}