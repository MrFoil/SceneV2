 //
// Created by DIko on 26.06.2015.
//

#include "Wall.h"

Wall::Wall(vec3 a, vec3 b, vec3 c, vec3 d, GLuint shaderId, GLuint texDim) {
    const GLuint vCount = 4;
    mesh = new Vertex[vCount];
    uint32_t ind[] ={
            0, 1, 2,   2, 3, 0
    };

    this->shaderId = shaderId;

    mesh[0].pos = a;
    mesh[1].pos = b;
    mesh[2].pos = c;
    mesh[3].pos = d;

    mesh[0].tex = vec2(0.0, 0.0);
    mesh[1].tex = vec2(texDim, 0.0);
    mesh[2].tex = vec2(texDim, texDim);
    mesh[3].tex = vec2(0.0, texDim);


    Wall::calcNormals(ind, 6, mesh, vCount);
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
    vbo = createVbo(mesh, vCount);
}

Wall::~Wall() {

}

GLuint Wall::createVbo(Vertex* data, GLuint length) {
    GLuint vboId;

    glBindVertexArray(this->vao);

    glGenBuffers(1, &vboId);
    glBindBuffer(GL_ARRAY_BUFFER, vboId);
    glBufferData(GL_ARRAY_BUFFER, length*sizeof(Vertex), data, GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    return vboId;
}

void Wall::calcNormals(GLuint* pIndices, GLuint IndexCount, Vertex* pVertices, GLuint VertexCount) {
    for (unsigned int i = 0 ; i < IndexCount ; i += 3) {
        unsigned int Index0 = pIndices[i];
        unsigned int Index1 = pIndices[i + 1];
        unsigned int Index2 = pIndices[i + 2];
        vec3 v1 = pVertices[Index1].pos - pVertices[Index0].pos;
        vec3 v2 = pVertices[Index2].pos - pVertices[Index0].pos;
        vec3 Normal = cross(v1, v2);

        normalize(Normal);

        pVertices[Index0].normal += Normal;
        pVertices[Index1].normal += Normal;
        pVertices[Index2].normal += Normal;
    }

    for (unsigned int i = 0 ; i < VertexCount ; i++) {
        normalize(pVertices[i].normal);
    }
}

void Wall::setTexture(char const *name) {
    this->textureId = loadBMP_custom(name);
}

void Wall::setLocation(vec4 loc) {
    this->translationId = glGetUniformLocation(shaderId, "translation");
    this->location = loc;
}

void Wall::render() {

    glUniform4f(translationId, location.x, location.y, location.z, location.w);

    glBindVertexArray(this->vao);
    glActiveTexture (GL_TEXTURE0);
    glBindTexture (GL_TEXTURE_2D, this->textureId);

    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glEnableVertexAttribArray(2);
    glBindBuffer(GL_ARRAY_BUFFER, this->vbo);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const GLvoid*)12);
    glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const GLvoid*)20);

    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

}


