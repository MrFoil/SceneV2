//
// Created by DIko on 17.06.2015.
//

#ifndef MY_SCENE_V__2_1_DIRLIGHT_H
#define MY_SCENE_V__2_1_DIRLIGHT_H

#include "../hdr/CommonHeaders.h"
using namespace std;
using namespace glm;

class DirLight {
private:
    vec4 direction;
    vec3 color;
    GLfloat ambInt, dirInt;

    GLint directionId, colorId, ambIntId, dirIntId;
public:
    DirLight(vec4 direction, vec3 color, GLfloat ambientInt, GLfloat dirInt, GLuint shaderId);
    ~DirLight();
    void render();
};


#endif //MY_SCENE_V__2_1_DIRLIGHT_H
