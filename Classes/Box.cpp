//
// Created by DIko on 24.05.2015.
//

#include "Box.h"

Box::Box(vec3 point1, vec3 point2, GLuint shaderId) {
    vec2 *texCoord = new vec2[6];
    //premixed texture coords, useful only in current case. Why 6? - 6 vertices per side.
    texCoord[0] = vec2(0.0f, 0.0f); texCoord[1] = vec2(1.0f, 0.0f); texCoord[2] = vec2(1.0f, 1.0f);
    texCoord[3] = vec2(1.0f, 1.0f); texCoord[4] = vec2(0.0f, 1.0f); texCoord[5] = vec2(0.0f, 0.0f);

    uint32_t ind[] = {
            0, 1, 2,   2, 3, 0, //front
            3, 2, 7,   7, 6, 3, //top
            5, 0, 3,   3, 6, 5, //left
            1, 4, 7,   7, 2, 1, //right
            5, 4, 1,   1, 0, 5, //bottom
            4, 5, 6,   6, 7, 4  //back
    };
    GLuint vertCount = sizeof(ind)/sizeof(ind[0]);
    //front
    this->vertices[0].pos = vec3(point1.x, point2.y, point1.z);
    this->vertices[1].pos = vec3(point2.x, point2.y, point1.z);
    this->vertices[2].pos = vec3(point2.x, point1.y, point1.z);
    this->vertices[3].pos = vec3(point1.x, point1.y, point1.z);
    //back
    this->vertices[4].pos = vec3(point2.x, point2.y, point2.z);
    this->vertices[5].pos = vec3(point1.x, point2.y, point2.z);
    this->vertices[6].pos = vec3(point1.x, point1.y, point2.z);
    this->vertices[7].pos = vec3(point2.x, point1.y, point2.z);
    //now we are having 8 points of our brick
    //computing normals:
    Box::calcNormals(ind, vertCount, this->vertices, 8);
    cout << "\n" << "Normal: " << vertices[1].normal.x << " "
                               << vertices[1].normal.y << " "
                               << vertices[1].normal.z << "\n";
    this->mesh = new Vertex[vertCount];
    for (int i=0, j=0; i<vertCount; i++, j++){
        if (j==6) j=0;
        GLuint index = ind[i];
        mesh[i].pos    = this->vertices[index].pos;
        mesh[i].normal = this->vertices[index].normal;
        mesh[i].tex    = texCoord[j];
    }

    /////////////////////////////////////////////////////
    this->shaderId = shaderId;
    glGenVertexArrays(1, &this->vao);
    glBindVertexArray(this->vao);
    this->vbo = createVbo(mesh, vertCount);

    delete [] texCoord; texCoord = nullptr;
}

Box::~Box() {
    delete [] mesh;
}

void Box::setTexture(char const *name) {
    this->textureId = loadBMP_custom(name);
}

void Box::setPosition(vec4 newPos) {
    this->translationId = glGetUniformLocation(shaderId, "translation");
    this->position = newPos;
}

void Box::calcNormals(GLuint* pIndices, GLuint IndexCount, Vertex* pVertices, unsigned int VertexCount) {
    for (unsigned int i = 0 ; i < IndexCount ; i += 3) {
        unsigned int Index0 = pIndices[i];
        unsigned int Index1 = pIndices[i + 1];
        unsigned int Index2 = pIndices[i + 2];
        vec3 v1 = pVertices[Index1].pos - pVertices[Index0].pos;
        vec3 v2 = pVertices[Index2].pos - pVertices[Index0].pos;
        vec3 Normal = cross(v1, v2);

        normalize(Normal);

        pVertices[Index0].normal += Normal;
        pVertices[Index1].normal += Normal;
        pVertices[Index2].normal += Normal;
    }

    for (unsigned int i = 0 ; i < VertexCount ; i++) {
        normalize(pVertices[i].normal);
    }
}

GLuint Box::createVbo(Vertex* data, GLuint length){
    GLuint vboId;

    glBindVertexArray(this->vao);

    glGenBuffers(1, &vboId);
    glBindBuffer(GL_ARRAY_BUFFER, vboId);
    glBufferData(GL_ARRAY_BUFFER, length*sizeof(Vertex), data, GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    return vboId;
}

void Box::applyMovement(Window &window, GLfloat speed, GLdouble time) {


    if (glfwGetKey( window.getWindow(), GLFW_KEY_RIGHT ) == GLFW_PRESS){
        this->position -= vec4(1.0f, 0.0f, 0.0f, 0.0f) * speed * (float)time;
    }

    if (glfwGetKey( window.getWindow(), GLFW_KEY_LEFT ) == GLFW_PRESS){
        this->position += glm::vec4 (1.0f, 0.0f, 0.0f, 0.0f) * speed * (float)time;

    }

    if (glfwGetKey( window.getWindow(), GLFW_KEY_UP ) == GLFW_PRESS){
        this->position += glm::vec4 (0.0f, 0.0f, 1.0f, 0.0f) * speed * (float)time;
    }

    if (glfwGetKey( window.getWindow(), GLFW_KEY_DOWN ) == GLFW_PRESS){
        this->position -= glm::vec4 (0.0f, 0.0f, 1.0f, 0.0f) * speed * (float)time;
    }

    if (glfwGetKey( window.getWindow(), GLFW_KEY_LEFT_CONTROL ) == GLFW_PRESS){
        this->position -= glm::vec4 (0.0f, 1.0f, 0.0f, 0.0f) * speed * (float)time;
    }

    if (glfwGetKey( window.getWindow(), GLFW_KEY_LEFT_SHIFT ) == GLFW_PRESS){
        this->position += glm::vec4 (0.0f, 1.0f, 0.0f, 0.0f) * speed * (float)time;
    }

}

void Box::render() {

    glUniform4fv (this->translationId, 1, &this->position[0]);

    glBindVertexArray(this->vao);
    glActiveTexture (GL_TEXTURE0);
    glBindTexture (GL_TEXTURE_2D, this->textureId);

    glBindBuffer(GL_ARRAY_BUFFER, this->vbo);
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glEnableVertexAttribArray(2);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const GLvoid*)12);
    glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const GLvoid*)20);

    glDrawArrays(GL_TRIANGLES, 0, 36);

    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

}