//
// Created by DIko on 24.05.2015.
//

#ifndef MY_SCENE_V__2_1_ENTITY_H
#define MY_SCENE_V__2_1_ENTITY_H


class Entity {
public:
    Entity() {};
    ~Entity() {};

    virtual void render() = 0;
};


#endif //MY_SCENE_V__2_1_ENTITY_H
