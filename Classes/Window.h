//
// Created by DIko on 26.05.2015.
//

#ifndef MY_SCENE4_WINDOW_H
#define MY_SCENE4_WINDOW_H
#include "../hdr/CommonHeaders.h"
using namespace glm;

class Window {
private:
    char const *name;
    vec4 color;
    GLFWwindow *window;
public:
    GLuint width, heigh;

    double screenCentX, screenCentY;

    Window(GLuint, GLuint, char const*, vec4);
    ~Window();
    int createWindow();
    bool closeWindow();

    GLFWwindow* getWindow();

};


#endif //MY_SCENE4_WINDOW_H
