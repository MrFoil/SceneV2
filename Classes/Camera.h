//
// Created by DIko on 27.05.2015.
//

#ifndef MY_SCENE_V__2_1_CAMERA_H
#define MY_SCENE_V__2_1_CAMERA_H

#include "../hdr/CommonHeaders.h"
#include "../Classes/Window.h"
using namespace glm;

class Camera {
private:
    mat4 viewMat, modelMat, projMat, mvp;
    vec3 pos;
    GLfloat camSpd, mouseSpd;
    GLdouble cursorStartX, cursorStartY, horAngle, vertAngle;
    bool RMB_DOWN;

    Window *window;
public:


    Camera(vec3 pos, Window *window);
    ~Camera();

    void calcMatrices(GLdouble dTime);

    mat4 getViewMat();
    mat4 getModelMat();
    mat4 getProjMat();
    mat4 getMVP();

    void pushMatrix(mat4 matrix, const char *, GLuint shaderId);
    void sendMVP(GLuint);


};


#endif //MY_SCENE_V__2_1_CAMERA_H
