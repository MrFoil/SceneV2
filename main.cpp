#include "hdr/CommonHeaders.h"
#include "hdr/CommonMath.h"

#include "Classes/Entity.h"
#include "Classes/Box.h"
#include "Classes/Window.h"
#include "Classes/Camera.h"
#include "Classes/DirLight.h"
#include "Classes/Wall.h"

using namespace std;


int main() {
    Window window(800, 600, "Test", vec4(0.0, 0.0, 0.0 ,0.0));
    window.createWindow();
    Camera camera(vec3(0.0f, 0.0f, -5.0f), &window);

    GLuint programID = LoadShaders(
            "vshader.vertex",
            "fshader.fragment"
    );

    Wall wall(vec3(5.0f, -2.0f, -4.0f), vec3(-5.0f, -2.0f, -4.0f), vec3(-5.0f, 0.0f, 4.0f), vec3(5.0f, 0.0f, 4.0f), programID, 6);
    wall.setLocation(vec4(0.0f, 0.0f, 2.0f, 0.0f));
    wall.setTexture("portal.bmp");

    Box box(vec3(0.5f, 1.0f, -0.5f), vec3(-0.5f, -1.0f, 0.5f), programID);
    box.setPosition(vec4(0.0f, 0.0f, 0.0f, 1.0f));
    box.setTexture("portal.bmp");

    Box box2(vec3(0.5f, 0.5f, -0.5f), vec3(-0.5f, -0.5f, 0.5f), programID);
    box2.setPosition(vec4(4.0f, 0.0f, 0.0f, 1.0f));
    box2.setTexture("Hat.bmp");

    DirLight light(vec4(0.0f, 1.0f, -1.0f, 0.0f), vec3(1.0f, 1.0f, 1.0f), 0.05f, 0.7f, programID);

    GLdouble dTime=0, startTime=0;
    do{
        startTime = glfwGetTime();
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        camera.calcMatrices(dTime);

        camera.pushMatrix(camera.getMVP(), "MVP", programID);
        box2.render();
        wall.render();
        box.render();
        box.applyMovement(window, 10.0f, dTime);

        light.render();

        glfwSwapBuffers(window.getWindow());
        glfwPollEvents();

        dTime = glfwGetTime() - startTime;

    } while(!window.closeWindow());

    return 0;
}