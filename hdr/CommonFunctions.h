//
// Created by DIko on 08.06.2015.
//

#ifndef MY_SCENE_V__2_1_COMMONFUNCTIONS_H
#define MY_SCENE_V__2_1_COMMONFUNCTIONS_H
#include "CommonHeaders.h"

GLuint LoadShaders(const char * vertex_file_path,const char * fragment_file_path);

GLuint loadBMP_custom(const char * imagepath);


#endif //MY_SCENE_V__2_1_COMMONFUNCTIONS_H
