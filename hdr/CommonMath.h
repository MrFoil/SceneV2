//
// Created by DIko on 27.05.2015.
//

#ifndef MY_SCENE_V__2_1_COMMONMATH_H
#define MY_SCENE_V__2_1_COMMONMATH_H
#include "CommonHeaders.h"
using namespace glm;

struct Vertex
{
    glm::vec3 pos;
    glm::vec2 tex;
    glm::vec3 normal = glm::vec3(0.0f, 0.0f, 0.0f);

    Vertex() {}

    Vertex(glm::vec3 pos, glm::vec2 tex)
    {
        this->pos = pos;
        this->tex = tex;
        this->normal = glm::vec3(0.0f, 0.0f, 0.0f);
    }
};

class TexDesc{
private:
    vec2 pos;
    GLuint ind;
    GLuint *texInd;
public:
    GLuint getPos();
};

struct Ambient{
    vec4 color;
    GLfloat intencity;

    GLint colorId, intencityId;
};

struct Diffuse{
    vec4 pos;
    vec4 color;
    GLfloat intencity;

    GLint posId, colorId, intencityId;
};



#endif //MY_SCENE_V__2_1_COMMONMATH_H
