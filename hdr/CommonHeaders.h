//
// Created by DIko on 26.05.2015.
//

#ifndef MY_SCENE4_COMMONHEADERS_H
#define MY_SCENE4_COMMONHEADERS_H

#include <string>
#include <fstream>
#include <vector>

#include <GL/glew.h>
#include <GL/glu.h>

#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <stdlib.h>
#include <iostream>
#include <stdio.h>

#endif //MY_SCENE4_COMMONHEADERS_H
